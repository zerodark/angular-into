import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {BehaviorSubject} from 'rxjs';
import {DemoServiceService} from './demo-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  public title = 'progra3 !!!';
  public contador = 0;

  public miobservable: BehaviorSubject<any> = new BehaviorSubject<any>(0);

  constructor(private router: Router,
              private myService: DemoServiceService
  ) {
    this.miobservable
      .asObservable()
      .subscribe((result) => {
        this.title = this.title + result;
      });
  }

  public navegacion01(): void {
    this.router.navigate(['/ejemplo']);
  }

  public navegacion02(): void {
    this.router.navigate(['/ejemplo2']);
  }

  public cambiarTitulo(): void {
    this.contador++;
    this.miobservable.next(this.contador);
    // this.title = this.contador
  }

  public callWebService(): void {
    this.myService.getDataFromServer().subscribe(result => {
      console.log(result);
      this.miobservable.next(result.message);

    });
  }

}
