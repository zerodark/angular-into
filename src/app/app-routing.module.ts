import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {EjemploComponent} from './ejemplo/ejemplo.component';
import {Ejemplo2Component} from './ejemplo2/ejemplo2.component';

const routes: Routes = [
  {
    path: 'ejemplo',
    component: EjemploComponent
  },
  {
    path: 'ejemplo2',
    component: Ejemplo2Component
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
