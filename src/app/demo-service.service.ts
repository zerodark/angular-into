import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DemoServiceService {

  constructor(private httpClient: HttpClient) {
  }

  public getDataFromServer(): Observable<any> {
    const myHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Accept: 'application/json'
    });
    const url = 'http://localhost:8586/persona/dummy';
    return this.httpClient.get(url, {headers: myHeaders});
  }

}
