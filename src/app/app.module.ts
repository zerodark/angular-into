import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';


import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {EjemploComponent} from './ejemplo/ejemplo.component';
import {Ejemplo2Component} from './ejemplo2/ejemplo2.component';


@NgModule({
  declarations: [
    AppComponent,
    EjemploComponent,
    Ejemplo2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
